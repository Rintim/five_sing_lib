defmodule FiveSing.MixProject do
  use Mix.Project

  def project do
    [
      app: :five_sing,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: escript()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:standard, git: "https://gitlab.com/Theopse/standard.git", branch: :Development},
      {:download, git: "https://gitlab.com/Theopse/download.git", branch: :Development},
      {:esolicit, git: "https://gitlab.com/Theopse/esolicit.git", branch: :Development},
      {:temp, "~> 0.4.7"}
    ]
  end
  defp escript, do: [
    main_module: FiveSing.CLI
  ]
end
